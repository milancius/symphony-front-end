import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://192.168.0.16:8000/';

class HotelsService {
    getAllHotels() {
        return axios.get(API_URL + 'hotel_api/', { headers: authHeader() });
    }

    getHotelDetails(id) {
        return axios.get(API_URL + 'hotel_api/' + id + '/', { headers: authHeader() });
    }

    getHotelReviews(id) {
        return axios.get(API_URL + 'hotel_api/get_hotel_reviews/' + id + '/', { headers: authHeader() });
    }

    getFavoriteHotels() {
        return axios.get(API_URL + 'favorites/', { headers: authHeader() });
    }

    addNewHotel(hotel) {
        return axios.post(API_URL + 'hotel_api/', {
            name: hotel.name,
            description: hotel.description,
            price: hotel.price,
            user: hotel.user
        }, { headers: authHeader() })
        .then(response => {
            return response.data;
        });
    }

    addRemoveFavoriteHotel(hotel) {
        return axios.post(API_URL + 'favorites/add_remove', {
            hotel_id: hotel.hotel_id,
            is_favorite: hotel.is_favorite
        }, { headers: authHeader() })
        .then(response => {
            return response.data;
        });
    }
}

export default new HotelsService();