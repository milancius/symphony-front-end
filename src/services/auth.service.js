import axios from 'axios';

const API_URL = 'http://192.168.0.16:8000/';

class AuthService {
    login(user) {
        return axios
            .post(API_URL + 'api-token-auth/', {
                username: user.username,
                password: user.password
            })
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register/', {
            username: user.username,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();