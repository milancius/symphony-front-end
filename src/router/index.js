import Vue from 'vue'
import VueRouter from 'vue-router';

import Login from '../views/Login'
import SignUp from "../views/SignUp";
import Dashboard from "../views/Dashboard";
import Favorites from "../views/Favorites";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login,
        },
        {
            path: '/sign-up',
            name: 'SignUp',
            component: SignUp,
        },
        {
            path: '/home',
            name: 'Home',
            component: Dashboard
        },
        {
            path: '/favorites',
            name: 'Favorites',
            component: Favorites
        }
    ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/', '/sign-up'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    next({name: 'Login'});
  } else {
    next();
  }
});

export default router;