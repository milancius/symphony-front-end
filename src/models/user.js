export default class User {
    constructor(username, last_name, first_name, email, password) {
        this.username = username;
        this.last_name = last_name;
        this.first_name = first_name
        this.email = email;
        this.password = password;
    }
}